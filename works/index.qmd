---
title: "Works"
listing:
  - id: work_ems
    contents: work_ems.yml
    sort: "date desc"
    template: ../html/works/listing.ejs
  - id: work_ird
    contents: work_ird.yml
    sort: "date desc"
    template: ../html/works/listing.ejs
  - id: work_personnal
    contents: work_personnal.yml
    sort: "date desc"
    template: ../html/works/listing.ejs 
page-layout: full
toc: true
toc-location: left
toc-title: Institution
title-block-banner: true
---

## Personnal

:::{#work_personnal}
:::

## At Eurometropolis of Strasbourg

:::{#work_ems}
:::

## At The French National Research Institute for Sustainable Development

:::{#work_ird}
:::